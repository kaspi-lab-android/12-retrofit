package kz.company.lesson_12.model

data class Currency(
    val rates: Map<String, Double>,
    val base: String,
    val date: String
)
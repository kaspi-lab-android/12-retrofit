package kz.company.lesson_12.api

import io.reactivex.Single
import kz.company.lesson_12.model.Currency
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

const val key = "LAKSJD*IIYIDA*S&DO(AD{AS)"
interface ExchangeApiService {
    //private val url = "https://api.exchangeratesapi.io/latest"
    companion object {
        fun create(): ExchangeApiService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://api.exchangeratesapi.io/")
                .build()

            return retrofit.create(ExchangeApiService::class.java)
        }
    }

    @GET("latest")
    fun getLatest(): Single<Currency>

    @GET("translate")
    fun getTranslate(
        @Query("text") text: String,
        @Query("lang") langDirection: String
    ): Single<Currency>

    //www.url.com/translate?text=text&lang=langDirection

    /*@POST("change_user")
    fun changeUser(name: String)*/
}
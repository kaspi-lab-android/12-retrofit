package kz.company.lesson_12

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kz.company.lesson_12.api.ExchangeApiService

class MainActivity : AppCompatActivity() {

    private val exchangeApiService = ExchangeApiService.create()
    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getData()
    }

    private fun getData(){
        exchangeApiService.getLatest()
            .subscribeOn(Schedulers.io()) //Подписаться
            .observeOn(AndroidSchedulers.mainThread()) //Наблюдать
            .subscribe(
                {
                    textView.text = it.rates.toString()
                },
                {
                    Log.e(it.message.toString(), "Error get currency")
                }
            )
            .also { disposable }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }
}
